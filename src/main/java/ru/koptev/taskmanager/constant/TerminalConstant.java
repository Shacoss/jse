package ru.koptev.taskmanager.constant;

public class TerminalConstant {

    public static final String ABOUT = "about";
    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String EXIT = "exit";
    public static final String COMMAND_HISTORY = "command-history";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String PROJECT_VIEW_BY_NAME = "project-view-by-name";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";
    public static final String PROJECT_REMOVE_WITH_TASKS = "project-remove-with-tasks";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String TASK_VIEW_BY_NAME = "task-view-by-name";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String TASK_LIST_BY_PROJECT_ID = "task-list-by-project-id";
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";
    public static final String TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";

    public static final String USER_AUTHENTICATION = "user-authentication";
    public static final String USER_LOGOFF = "user-logoff";
    public static final String USER_CREATE = "user-create";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_LIST = "user-list";
    public static final String USER_VIEW_CURRENT_PROFILE = "user-view-current-profile";
    public static final String USER_VIEW_BY_INDEX = "user-view-by-index";
    public static final String USER_VIEW_BY_ID = "user-view-by-id";
    public static final String USER_REMOVE_BY_INDEX = "user-remove-by-index";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String USER_UPDATE_CURRENT_PROFILE = "user-update-current-profile";
    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";
    public static final String USER_UPDATE_BY_ID = "user-update-by-id";

}
