package ru.koptev.taskmanager.enumerated;


public enum Role {

    ADMIN("Administrator"),
    USER("User");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Role getRoleFromString(final String role) {
        try {
            return Role.valueOf(role);
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }

    @Override
    public String toString() {
        return displayName;
    }

}
