package ru.koptev.taskmanager.service;

import ru.koptev.taskmanager.entity.Task;
import ru.koptev.taskmanager.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name, userId);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name, description, userId);
    }

    public void clear(final Long userId) {
        taskRepository.clear(userId);
    }

    public List<Task> findAll(final Long userId) {
        return taskRepository.findAll(userId);
    }

    public Task findById(final Long taskId, final Long userId) {
        if (taskId == null) return null;
        return taskRepository.findById(taskId, userId);
    }

    public List<Task> findByName(final String taskName) {
        if (taskName == null || taskName.isEmpty()) return null;
        return taskRepository.findByName(taskName);
    }

    public Task removeById(final Long taskId, final Long userId) {
        if (taskId == null) return null;
        return taskRepository.removeById(taskId, userId);
    }

    public List<Task> removeByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name, userId);
    }

    public Task updateById(final Long taskId, final String name, final String description, final Long userId) {
        if (taskId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.updateById(taskId, name, description, userId);
    }

}
