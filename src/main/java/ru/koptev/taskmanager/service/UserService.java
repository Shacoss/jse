package ru.koptev.taskmanager.service;

import ru.koptev.taskmanager.enumerated.Role;
import ru.koptev.taskmanager.entity.User;
import ru.koptev.taskmanager.repository.UserRepository;
import ru.koptev.taskmanager.util.HashUtil;

import java.util.List;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User authentication(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return userRepository.authentication(login, HashUtil.saltHashMD5(password));
    }

    public User create(final String login, final String password, final String roleString) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleString == null || roleString.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        if (role == null) return null;
        return userRepository.create(login, HashUtil.saltHashMD5(password), role);
    }

    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return userRepository.create(login, HashUtil.saltHashMD5(password));
    }

    public void clear() {
        userRepository.clear();
    }

    public int size() {
        return userRepository.size();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByIndex(final int index) {
        if (index < 0 || index > userRepository.size() - 1) return null;
        return userRepository.findByIndex(index);
    }

    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User removeByIndex(final int index) {
        if (index < 0 || index > userRepository.size() - 1) return null;
        return userRepository.removeByIndex(index);
    }

    public User removeById(final Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public User updateById(final Long id, final String password, String fistName, String lastName, final String roleString) {
        if (id == null) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleString == null || roleString.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        if (role == null) return null;
        if (fistName == null || fistName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        return userRepository.updateById(id, HashUtil.saltHashMD5(password), role, fistName, lastName);
    }

    public User updateByLogin(final String login, final String password, final String fistName,
                              final String lastName, final String roleString
    ) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleString == null || roleString.isEmpty()) return null;
        Role role = getRoleFromString(roleString);
        if (role == null) return null;
        if (fistName == null || fistName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        return userRepository.updateByLogin(login, HashUtil.saltHashMD5(password), role, fistName, lastName);
    }

    private Role getRoleFromString(final String roleString) {
        try {
            return Role.valueOf(roleString);
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }

}
