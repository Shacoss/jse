package ru.koptev.taskmanager.service;

import ru.koptev.taskmanager.entity.Project;
import ru.koptev.taskmanager.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, userId);
    }

    public Project create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description, userId);
    }

    public void clear(final Long userId) {
        projectRepository.clear(userId);
    }

    public List<Project> findAll(final Long userId) {
        return projectRepository.findAll(userId);
    }

    public Project findById(final Long projectId, final Long userId) {
        if (projectId == null) return null;
        return projectRepository.findById(projectId, userId);
    }

    public List<Project> findByName(final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        return projectRepository.findByName(projectName);
    }

    public Project removeById(final Long projectId, final Long userId) {
        if (projectId == null) return null;
        return projectRepository.removeById(projectId, userId);
    }

    public List<Project> removeByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name, userId);
    }


    public Project updateById(final Long projectId, final String name, final String description, final Long userId) {
        if (projectId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.updateById(projectId, name, description, userId);
    }

}
