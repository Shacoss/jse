package ru.koptev.taskmanager.service;

import ru.koptev.taskmanager.entity.Project;
import ru.koptev.taskmanager.entity.Task;
import ru.koptev.taskmanager.repository.ProjectRepository;
import ru.koptev.taskmanager.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    public Task addTaskToProject(final Long projectId, final Long taskId, final Long userId) {
        if (projectId == null) return null;
        Project project = projectRepository.findById(projectId, userId);
        if (project == null) return null;
        Task task = taskRepository.findById(taskId, userId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId, final Long userId) {
        if (projectId == null) return null;
        Task task = taskRepository.findByProjectIdAndId(projectId, taskId, userId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Project removeProjectWithTasks(final Long projectId, final Long userId) {
        if (projectId == null) return null;
        Project project = projectRepository.findById(projectId, userId);
        if (project == null) return null;
        projectRepository.removeById(projectId, userId);
        List<Task> tasks = findAllByProjectId(projectId, userId);
        if (tasks == null) return project;
        for (Task task : tasks) {
            taskRepository.removeById(task.getId(), userId);
        }
        return project;
    }

}
