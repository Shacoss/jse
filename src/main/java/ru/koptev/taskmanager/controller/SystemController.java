package ru.koptev.taskmanager.controller;

import java.util.LinkedList;
import java.util.List;

public class SystemController {

    private List<String> commands;

    public SystemController() {
        this.commands = new LinkedList<>();
    }

    public void displayHelp() {
        System.out.println("System commands:");
        System.out.println("about - Display developer information.");
        System.out.println("version - Display program version.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println("command-history - Display the last 10 user commands.");
        System.out.println();
        System.out.println("Project commands:");
        System.out.println("project-create - Create new project.");
        System.out.println("project-clear - Clear all projects.");
        System.out.println("project-list - Display all projects.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-remove-by-id - Delete project by id.");
        System.out.println("project-remove-by-name - Delete project by name");
        System.out.println("project-update-by-id - Update project by id.");
        System.out.println("project-remove-with-tasks - Remove project with task.");
        System.out.println();
        System.out.println("Task commands:");
        System.out.println("task-create - Create new task.");
        System.out.println("task-clear - Clear all tasks.");
        System.out.println("task-list - Display all tasks.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-remove-by-id - Delete task by id.");
        System.out.println("task-remove-by-name - Delete task by name.");
        System.out.println("task-update-by-id - Update task by id.");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println();
        System.out.println("User commands:");
        System.out.println("user-authentication - Authentication.");
        System.out.println("user-logoff - Log off.");
        System.out.println("user-create - Create new user.");
        System.out.println("user-clear - Clear all users.");
        System.out.println("user-view-current-profile - Display user profile.");
        System.out.println("user-list - Display all users.");
        System.out.println("user-view-by-id - Display user by id.");
        System.out.println("user-remove-by-id - Delete user by id.");
        System.out.println("user-remove-by-name - Delete user by name");
        System.out.println("user-update-current-profile - Update user profile.");
        System.out.println("user-update-by-login - Update user by login.");
        System.out.println("user-update-by-id - Update user by id.");
    }

    public void displayExit() {
        System.out.println("Terminate program...");
    }

    public void displayAbout() {
        System.out.println("Name: Koptev Anton");
        System.out.println("Email: koptev_av@nlmk.com");
    }

    public void displayVersion() {
        System.out.println("Version: 1.0.13");
    }

    public void displayError() {
        System.out.println("Error! Invalid parameter...");
    }

    public void displayWelcome() {
        System.out.println("Welcome to Task Manager");
    }

    public void commandHistory() {
        System.out.println("Commands history:");
        for (String command : commands) {
            System.out.println(command);
        }
    }

    public void logCommand(String command) {
        if (commands.size() == 10) commands.remove(0);
        commands.add(command);
    }

}
