package ru.koptev.taskmanager.controller;

import java.util.Scanner;

abstract class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

    protected Long parseLong(String id) {
        try {
            return Long.parseLong(id);
        } catch (NumberFormatException exception) {
            return null;
        }
    }

    protected boolean checkAuth(Long session){
        boolean checkAuth = session == null;
        if(checkAuth){
            System.out.println("Access denied!");
        }
        return checkAuth;
    }

}
