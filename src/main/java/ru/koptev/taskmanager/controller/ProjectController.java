package ru.koptev.taskmanager.controller;

import ru.koptev.taskmanager.entity.Project;
import ru.koptev.taskmanager.service.ProjectService;
import ru.koptev.taskmanager.service.ProjectTaskService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectController extends AbstractController {

    private ProjectService projectService;

    private ProjectTaskService projectTaskService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    public void create(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Creating project");
        System.out.println("Please, enter project name...");
        String name = scanner.nextLine();
        Project project = projectService.create(name, session);
        if (project == null) {
            System.out.println("Empty project name!");
            return;
        }
        System.out.println("Project successfully created!");
    }

    public void clear(final Long session) {
        if (checkAuth(session)) return;
        projectService.clear(session);
        System.out.println("Project clear!");
    }

    public void findAll(final Long session) {
        if (checkAuth(session)) return;
        int index = 1;
        List<Project> projects = projectService.findAll(session);
        Collections.sort(projects, Comparator.comparing(Project::getName));
        System.out.println("Project list:");
        System.out.format("%-10s%-20s%-20s%-20s%-20s%n", "Index", "Id", "Name", "Description", "User Id");
        for (Project project : projects) {
            System.out.printf("%-10d%-20d%-20s%-20s%-20s%n", index, project.getId(), project.getName(), project.getDescription(), project.getUserId());
            index++;
        }
    }

    public void view(final Project project) {
        if (project == null) {
            System.out.println("Project not found!");
            return;
        }
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("User Id: " + project.getUserId());
    }

    public void viewList(final List<Project> projects) {
        if (projects == null || projects.isEmpty()) {
            System.out.println("Project not found!");
            return;
        }
        System.out.format("%-20s%-20s%-20s%-20s%n", "Id", "Name", "Description", "User Id");
        for (Project project : projects) {
            System.out.printf("%-20d%-20s%-20s%-20s%n", project.getId(), project.getName(), project.getDescription(), project.getUserId());
        }
    }

    public void viewById(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Please, enter project id...");
        Long id = parseLong(scanner.nextLine());
        Project project = projectService.findById(id, session);
        view(project);
    }

    public void viewByName(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Please, enter project name...");
        String name = scanner.nextLine();
        List<Project> projects = projectService.findByName(name);
        viewList(projects);
    }

    public void removeById(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Deleting a project by id");
        System.out.println("Please, enter project id...");
        Long id = parseLong(scanner.nextLine());
        Project removeProject = projectService.removeById(id, session);
        if (removeProject != null) {
            System.out.println("Project successfully deleted by id");
            return;
        }
        System.out.println("Error, the project was not deleted!");
    }

    public void removeByName(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Deleting a project by name");
        System.out.println("Please, enter project name...");
        String name = scanner.nextLine();
        List<Project> removeProject = projectService.removeByName(name, session);
        if (removeProject != null || !removeProject.isEmpty()) {
            System.out.println("Project successfully deleted by name");
            return;
        }
        System.out.println("Error, the project was not deleted!");
    }

    public void updateById(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Updating a project by id");
        System.out.println("Please, enter project id...");
        Long id = parseLong(scanner.nextLine());
        System.out.println("Please, enter new project name...");
        String name = scanner.nextLine();
        System.out.println("Please, enter new project description...");
        String description = scanner.nextLine();
        Project updatedProject = projectService.updateById(id, name, description, session);
        if (updatedProject != null) {
            System.out.println("Project successfully updated by id");
            return;
        }
        System.out.println("Error, the project is not updated!");
    }

    public void removeProjectWithTasks(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Remove project with task");
        System.out.println("Please, enter project id...");
        Long id = parseLong(scanner.nextLine());
        Project project = projectTaskService.removeProjectWithTasks(id, session);
        if (project != null) {
            System.out.println("Project successfully deleted with tasks");
            return;
        }
        System.out.println("Error, the project was not deleted!");
    }

}
