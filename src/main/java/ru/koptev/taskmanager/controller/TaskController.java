package ru.koptev.taskmanager.controller;

import ru.koptev.taskmanager.entity.Task;
import ru.koptev.taskmanager.service.ProjectTaskService;
import ru.koptev.taskmanager.service.TaskService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    public void create(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Creating task");
        System.out.println("Please, enter task name...");
        String name = scanner.nextLine();
        Task task = taskService.create(name, session);
        if (task == null) {
            System.out.println("Empty task name!");
            return;
        }
        System.out.println("Task successfully created!");
    }

    public void clear(final Long session) {
        if (checkAuth(session)) return;
        taskService.clear(session);
        System.out.println("Task clear!");
    }

    public void findAll(final Long session) {
        if (checkAuth(session)) return;
        int index = 1;
        List<Task> tasks = taskService.findAll(session);
        Collections.sort(tasks, Comparator.comparing(Task::getName));
        System.out.println("Tasks list:");
        System.out.format("%-10s%-20s%-20s%-20s%-20s%n", "Index", "Id", "Name", "Description", "User Id");
        for (Task task : tasks) {
            System.out.printf("%-10d%-20d%-20s%-20s%-20s%n", index, task.getId(), task.getName(), task.getDescription(), task.getUserId());
            index++;
        }
    }

    public void view(final Task task) {
        if (task == null) {
            System.out.println("Task not found!");
            return;
        }
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("User Id: " + task.getUserId());
    }

    public void viewList(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Task not found!");
            return;
        }
        System.out.format("%-20s%-20s%-20s%-20s%n", "Id", "Name", "Description", "User Id");
        for (Task task : tasks) {
            System.out.printf("%-20d%-20s%-20s%-20s%n", task.getId(), task.getName(), task.getDescription(), task.getUserId());
        }
    }

    public void viewById(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Please, enter task id...");
        Long id = parseLong(scanner.nextLine());
        view(taskService.findById(id, session));
    }

    public void viewByName(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Please, enter task name...");
        String name = scanner.nextLine();
        List<Task> tasks = taskService.findByName(name);
        viewList(tasks);
    }

    public void removeById(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Deleting a task by id");
        System.out.println("Please, enter task id...");
        Long id = parseLong(scanner.nextLine());
        Task removeTask = taskService.removeById(id, session);
        if (removeTask != null) {
            System.out.println("Task successfully deleted by id");
            return;
        }
        System.out.println("Error, the task was not deleted!");
    }

    public void removeByName(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Deleting a task by name");
        System.out.println("Please, enter task name...");
        String name = scanner.nextLine();
        List<Task> removeTask = taskService.removeByName(name, session);
        if (removeTask != null || !removeTask.isEmpty()) {
            System.out.println("Task successfully deleted by name");
            return;
        }
        System.out.println("Error, the task was not deleted!");
    }

    public void updateById(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Updating a task by id");
        System.out.println("Please, enter task id...");
        Long id = parseLong(scanner.nextLine());
        System.out.println("Please, enter new task name...");
        String name = scanner.nextLine();
        System.out.println("Please, enter new task description...");
        String description = scanner.nextLine();
        Task updatedTask = taskService.updateById(id, name, description, session);
        if (updatedTask != null) {
            System.out.println("Task successfully updated by id");
            return;
        }
        System.out.println("Error, the task is not updated!");
    }

    public void listTaskByProjectIds(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("View tasks by project id");
        System.out.println("Please, enter project id...");
        Long id = parseLong(scanner.nextLine());
        List<Task> tasks = projectTaskService.findAllByProjectId(id, session);
        System.out.println("Tasks list:");
        System.out.format("%-20s%-20s%-20s%-20s%n", "Id", "Name", "Description", "User Id");
        for (Task task : tasks) {
            System.out.printf("%-20d%-20s%-20s%-20s%n", task.getId(), task.getName(), task.getDescription(), task.getUserId());
        }
    }

    public void addTaskToProjectByIds(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Add task to project");
        System.out.println("Please, enter project id...");
        Long projectId = parseLong(scanner.nextLine());
        System.out.println("Please, enter task id...");
        Long taskId = parseLong(scanner.nextLine());
        Task task = projectTaskService.addTaskToProject(projectId, taskId, session);
        if (task == null) {
            System.out.println("The task was not added to the project!");
            return;
        }
        System.out.println("The task was successfully added to the project!");
    }

    public void removeTaskFromProjectByIds(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Remove task from project");
        System.out.println("Please, enter project id...");
        Long projectId = parseLong(scanner.nextLine());
        System.out.println("Please, enter task id...");
        Long taskId = parseLong(scanner.nextLine());
        Task task = projectTaskService.removeTaskFromProject(projectId, taskId, session);
        if (task == null) {
            System.out.println("The task was not remove to the project!");
            return;
        }
        System.out.println("The task was successfully remove to the project!");
    }

}
