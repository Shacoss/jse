package ru.koptev.taskmanager.controller;

import ru.koptev.taskmanager.entity.User;
import ru.koptev.taskmanager.service.UserService;

public class UserController extends AbstractController {

    private UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;
    }

    public Long authentication() {
        System.out.println("Authentication");
        System.out.println("Please, enter login...");
        String login = scanner.nextLine();
        System.out.println("Please, enter password...");
        String password = scanner.nextLine();
        User user = userService.authentication(login, password);
        if (user != null) {
            System.out.println("Authentication succeeded!");
            return user.getId();
        }
        System.out.println("Authentication failed!");
        return null;
    }

    public Long logOff(Long session) {
        System.out.println("Log Off");
        if (session != null) {
            session = null;
            System.out.println("Log Off succeeded!");
            return session;
        }
        System.out.println("Log Off failed!");
        return null;
    }

    public void viewProfile(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("User profile:");
        User user = userService.findById(session);
        view(user);
    }

    public void create() {
        System.out.println("Creating user");
        System.out.println("Please, enter login...");
        String login = scanner.nextLine();
        System.out.println("Please, enter password...");
        String password = scanner.nextLine();
        System.out.println("Please, enter role...");
        String role = scanner.nextLine();
        User user = userService.create(login, password, role);
        if (user == null) {
            System.out.println("User not created!");
            return;
        }
        System.out.println("User successfully created!");
    }

    public void clear() {
        userService.clear();
        System.out.println("Users clear!");
    }

    public void list() {
        int index = 1;
        System.out.println("Users list:");
        System.out.format("%-10s%-20s%-20s%-20s%-20s%-20s%-20s%n", "Index", "Id", "Login", "Fist Name", "Last Name", "Role", "Hash Password");
        for (User user : userService.findAll()) {
            System.out.printf("%-10d%-20d%-20s%-20s%-20s%-20s%-20s%n", index, user.getId(), user.getLogin(), user.getFistName(), user.getLastName(), user.getRole(), user.getPassword());
            index++;
        }
    }

    public void view(final User user) {
        if (user == null) {
            System.out.println("User not found!");
            return;
        }
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Fist Name: " + user.getFistName());
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("Role: " + user.getRole());
        System.out.println("Hash Password: " + user.getPassword());
    }

    public void viewByIndex() {
        System.out.println("Please, enter user index...");
        int index = Integer.parseInt(scanner.nextLine()) - 1;
        view(userService.findByIndex(index));
    }

    public void viewById() {
        System.out.println("Please, enter user id...");
        Long id = parseLong(scanner.nextLine());
        User user = userService.findById(id);
        view(user);
    }

    public void removeById() {
        System.out.println("Deleting a user by id");
        System.out.println("Please, enter user id...");
        Long id = parseLong(scanner.nextLine());
        User removeUser = userService.removeById(id);
        if (removeUser != null) {
            System.out.println("User successfully deleted by id");
            return;
        }
        System.out.println("Error, the user was not deleted!");
    }

    public void removeByIndex() {
        System.out.println("Deleting a user by index");
        System.out.println("Please, enter user index...");
        int index = Integer.parseInt(scanner.nextLine()) - 1;
        User removeUser = userService.removeByIndex(index);
        if (removeUser != null) {
            System.out.println("User successfully deleted by index");
            return;
        }
        System.out.println("Error, the user was not deleted!");
    }

    public void removeByLogin() {
        System.out.println("Deleting a user by name");
        System.out.println("Please, enter login name...");
        String login = scanner.nextLine();
        User removeUser = userService.removeByLogin(login);
        if (removeUser != null) {
            System.out.println("User successfully deleted by name");
            return;
        }
        System.out.println("Error, the user was not deleted!");
    }

    public void updateProfile(final Long session) {
        if (checkAuth(session)) return;
        System.out.println("Updating profile");
        System.out.println("Please, enter new user password...");
        String password = scanner.nextLine();
        System.out.println("Please, enter new fist name...");
        String fistName = scanner.nextLine();
        System.out.println("Please, enter new last name...");
        String lastName = scanner.nextLine();
        System.out.println("Please, enter role...");
        String role = scanner.nextLine();
        User updatedUser = userService.updateById(session, password, fistName, lastName, role);
        if (updatedUser != null) {
            System.out.println("User successfully updated by id");
            return;
        }
        System.out.println("Error, the user is not updated!");
    }

    public void updateById() {
        System.out.println("Updating a user by id");
        System.out.println("Please, enter user id...");
        Long id = parseLong(scanner.nextLine());
        System.out.println("Please, enter new user password...");
        String password = scanner.nextLine();
        System.out.println("Please, enter new fist name...");
        String fistName = scanner.nextLine();
        System.out.println("Please, enter new last name...");
        String lastName = scanner.nextLine();
        System.out.println("Please, enter role...");
        String role = scanner.nextLine();
        User updatedUser = userService.updateById(id, password, fistName, lastName, role);
        if (updatedUser != null) {
            System.out.println("User successfully updated by id");
            return;
        }
        System.out.println("Error, the user is not updated!");
    }

    public void updateByLogin() {
        System.out.println("Updating a user by login");
        System.out.println("Please, enter login...");
        String login = scanner.nextLine();
        System.out.println("Please, enter new user password...");
        String password = scanner.nextLine();
        System.out.println("Please, enter new fist name...");
        String fistName = scanner.nextLine();
        System.out.println("Please, enter new last name...");
        String lastName = scanner.nextLine();
        System.out.println("Please, enter role...");
        String role = scanner.nextLine();
        User updatedUser = userService.updateByLogin(login, password, fistName, lastName, role);
        if (updatedUser != null) {
            System.out.println("User successfully updated by login");
            return;
        }
        System.out.println("Error, the user is not updated!");
    }

}
