package ru.koptev.taskmanager.repository;

import ru.koptev.taskmanager.enumerated.Role;
import ru.koptev.taskmanager.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private List<User> users = new ArrayList<>();

    public User authentication(final String login, final String password) {
        for (User user : users) {
            if (!user.getLogin().equals(login)) continue;
            if (!user.getPassword().equals(password)) continue;
            return user;
        }
        return null;
    }

    public User create(final String login, final String password, final Role role,
                       final String fistName, final String lastName
    ) {
        User user = new User(login, password, role);
        user.setFistName(fistName);
        user.setLastName(lastName);
        users.add(user);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        User user = new User(login, password, role);
        users.add(user);
        return user;
    }

    public User create(final String login, final String password) {
        User user = new User(login, password);
        user.setRole(Role.USER);
        users.add(user);
        return user;
    }

    public void clear() {
        users.clear();
    }

    public int size() {
        return users.size();
    }

    public List<User> findAll() {
        return users;
    }

    public User findByIndex(final int index) {
        return users.get(index);
    }

    public User findById(final Long id) {
        for (User user : users) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }

    public User findByLogin(final String login) {
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }

    public User removeByIndex(final int index) {
        User user = findByIndex(index);
        users.remove(user);
        return user;
    }

    public User removeById(final Long id) {
        User user = findById(id);
        users.remove(user);
        return user;
    }

    public User removeByLogin(final String login) {
        User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateByLogin(final String login, final String password, final Role role,
                              final String fistName, final String lastName
    ) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setFistName(fistName);
        user.setLastName(lastName);
        user.setPassword(password);
        user.setRole(role);
        return user;
    }

    public User updateById(final Long id, final String password, final Role role,
                           final String fistName, final String lastName
    ) {
        User user = findById(id);
        if (user == null) return null;
        user.setFistName(fistName);
        user.setLastName(lastName);
        user.setPassword(password);
        user.setRole(role);
        return user;
    }

}
