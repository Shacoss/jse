package ru.koptev.taskmanager.repository;

import ru.koptev.taskmanager.entity.Project;
import ru.koptev.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();
    private Map<String, List<Task>> tasksMap = new HashMap<>();

    private void addMap(final Task task) {
        if (tasksMap.get(task.getName()) == null) {
            List<Task> tasks = new ArrayList<>();
            tasks.add(task);
            tasksMap.put(task.getName(), tasks);
            return;
        }
        if (tasksMap.get(task.getName()) != null) {
            tasksMap.get(task.getName()).add(task);
        }
    }

    private void removeFromMapById(final Long id, final Long userId) {
        for (Map.Entry<String, List<Task>> pair : tasksMap.entrySet()) {
            for (Task task : pair.getValue()) {
                if (!task.getUserId().equals(userId)) continue;
                if (!task.getId().equals(id)) continue;
                pair.getValue().remove(task);
                if (pair.getValue().isEmpty() || pair.getValue() == null) {
                    tasksMap.remove(pair.getKey());
                }
                return;
            }
        }
    }

    private void removeFromMapByName(final String name, final Long userId) {
        for (Map.Entry<String, List<Task>> pair : tasksMap.entrySet()) {
            for (Task task : pair.getValue()) {
                if (!task.getUserId().equals(userId)) continue;
                tasksMap.remove(name);
            }
        }
    }

    public Task create(final String name, final Long userId) {
        Task task = new Task(name);
        task.setUserId(userId);
        tasks.add(task);
        addMap(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        Task task = new Task(name);
        task.setDescription(description);
        task.setUserId(userId);
        tasks.add(task);
        addMap(task);
        return task;
    }

    public Task create(final String name, final String description) {
        Task task = new Task(name);
        task.setDescription(description);
        tasks.add(task);
        addMap(task);
        return task;
    }

    public void clear(final Long userId) {
        List<Task> userTasks = findByUserId(userId);
        for (Task task : userTasks) {
            tasks.remove(task);
        }
    }

    public List<Task> findAll(final Long userId) {
        List<Task> userTasks = findByUserId(userId);
        return userTasks;
    }

    public List<Task> findByName(final String taskName) {
        List<Task> result = tasksMap.get(taskName);
        return result;
    }

    public Task findById(final Long id, final Long userId) {
        for (Task task : tasks) {
            if (task.getUserId() == null) continue;
            if (!task.getId().equals(id)) continue;
            if (!task.getUserId().equals(userId)) continue;
            return task;
        }
        return null;
    }

    public List<Task> findByUserId(final Long userId) {
        List<Task> userTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getUserId() == null) continue;
            if (!task.getUserId().equals(userId)) continue;
            userTasks.add(task);
        }
        return userTasks;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long taskId, final Long userId) {
        for (Task task : tasks) {
            if (task.getProjectId() == null) continue;
            if (task.getUserId() == null) continue;
            if (!task.getUserId().equals(userId)) continue;
            if (!task.getProjectId().equals(projectId)) continue;
            if (!task.getId().equals(taskId)) continue;
            return task;
        }
        return null;
    }

    public Task removeById(final Long id, final Long userId) {
        Task task = findById(id, userId);
        tasks.remove(task);
        removeFromMapById(id, userId);
        return task;
    }

    public List<Task> removeByName(final String name, final Long userId) {
        List<Task> taskList = findByName(name);
        List<Task> removeList = new ArrayList<>();
        if (taskList == null || taskList.isEmpty()) return null;
        for (Task task : taskList) {
            if (task.getUserId() == null) continue;
            if (!task.getUserId().equals(userId)) continue;
            removeList.add(task);
            tasks.remove(task);
        }
        removeFromMapByName(name, userId);
        return removeList;
    }

    public Task updateById(final Long id, final String name, final String description, final Long userId) {
        for (Task task : tasks) {
            if (task.getUserId() == null) continue;
            if (!task.getUserId().equals(userId)) continue;
            if (!task.getId().equals(id)) continue;
            task.setName(name);
            task.setDescription(description);
            return task;
        }
        return null;
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll(userId)) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
