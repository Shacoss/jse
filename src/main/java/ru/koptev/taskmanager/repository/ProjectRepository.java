package ru.koptev.taskmanager.repository;

import ru.koptev.taskmanager.entity.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();
    private Map<String, List<Project>> projectsMap = new HashMap<>();

    private void addMap(final Project project) {
        if (projectsMap.get(project.getName()) == null) {
            List<Project> projects = new ArrayList<>();
            projects.add(project);
            projectsMap.put(project.getName(), projects);
            return;
        }
        if (projectsMap.get(project.getName()) != null) {
            projectsMap.get(project.getName()).add(project);
        }
    }

    private void removeFromMapById(final Long id, final Long userId) {
        for (Map.Entry<String, List<Project>> pair : projectsMap.entrySet()) {
            for (Project project : pair.getValue()) {
                if (!project.getUserId().equals(userId)) continue;
                if (!project.getId().equals(id)) continue;
                pair.getValue().remove(project);
                if (pair.getValue().isEmpty() || pair.getValue() == null) {
                    projectsMap.remove(pair.getKey());
                }
                return;
            }
        }
    }

    private void removeFromMapByName(final String name, final Long userId) {
        for (Map.Entry<String, List<Project>> pair : projectsMap.entrySet()) {
            for (Project project : pair.getValue()) {
                if (!project.getUserId().equals(userId)) continue;
                projectsMap.remove(name);
            }
        }
    }

    private void removeFromMapByUserId(final Long userId) {
        for (Map.Entry<String, List<Project>> pair : projectsMap.entrySet()) {
            for (Project project : pair.getValue()) {
                if (!project.getUserId().equals(userId)) continue;
                pair.getValue().remove(project);
                if (pair.getValue().isEmpty() || pair.getValue() == null) {
                    projectsMap.remove(pair.getKey());
                }
                return;
            }
        }
    }

    public Project create(final String name, final Long userId) {
        Project project = new Project(name);
        project.setUserId(userId);
        projects.add(project);
        addMap(project);
        return project;
    }

    public Project create(final String name, final String description) {
        Project project = new Project(name);
        project.setDescription(description);
        projects.add(project);
        addMap(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        Project project = new Project(name);
        project.setDescription(description);
        project.setUserId(userId);
        projects.add(project);
        addMap(project);
        return project;
    }

    public void clear(final Long userId) {
        List<Project> userProjects = findByUserId(userId);
        for (Project project : userProjects) {
            projects.remove(project);
        }
        removeFromMapByUserId(userId);
    }

    public List<Project> findAll(final Long userId) {
        List<Project> userProjects = findByUserId(userId);
        return userProjects;
    }

    public Project findById(final Long id, final Long userId) {
        for (Project project : projects) {
            if (project.getUserId() == null) continue;
            if (!project.getId().equals(id)) continue;
            if (!project.getUserId().equals(userId)) continue;
            return project;
        }
        return null;
    }

    public List<Project> findByName(final String projectName) {
        List<Project> result = projectsMap.get(projectName);
        return result;
    }

    private List<Project> findByUserId(final Long userId) {
        List<Project> userProjects = new ArrayList<>();
        for (Project project : projects) {
            if (project.getUserId() == null) continue;
            if (!project.getUserId().equals(userId)) continue;
            userProjects.add(project);
        }
        return userProjects;
    }

    public Project removeById(final Long id, final Long userId) {
        Project project = findById(id, userId);
        projects.remove(project);
        removeFromMapById(id, userId);
        return project;
    }

    public List<Project> removeByName(final String name, final Long userId) {
        List<Project> projectsList = findByName(name);
        List<Project> removeList = new ArrayList<>();
        if (projectsList == null || projectsList.isEmpty()) return null;
        for (Project project : projectsList) {
            if (project.getUserId() == null) continue;
            if (!project.getUserId().equals(userId)) continue;
            removeList.add(project);
            projects.remove(project);
        }
        removeFromMapByName(name, userId);
        return removeList;
    }

    public Project updateById(final Long id, final String name, final String description, final Long userId) {
        for (Project project : projects) {
            if (project.getUserId() == null) continue;
            if (!project.getUserId().equals(userId)) continue;
            if (!project.getId().equals(id)) continue;
            project.setName(name);
            project.setDescription(description);
            return project;
        }
        return null;
    }

}
