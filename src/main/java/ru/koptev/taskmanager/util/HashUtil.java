package ru.koptev.taskmanager.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {

    public static String saltHashMD5(final String password) {
        String passwordMD5 = md5(password);
        final String salt = "@$ghe!rbe,";
        for (int i = 0; i < 10; i++) {
            passwordMD5 = md5(salt + passwordMD5);
        }
        return passwordMD5;
    }

    private static String md5(final String inputValue) {
        try {
            byte[] bytesOfMessage = inputValue.getBytes("UTF-8");
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] digest = md5.digest(bytesOfMessage);
            String hash = new BigInteger(1, digest).toString(16);
            return hash;
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException exception) {
            throw new RuntimeException(exception.getMessage());
        }
    }

}
