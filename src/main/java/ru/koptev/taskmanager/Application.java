package ru.koptev.taskmanager;

import ru.koptev.taskmanager.controller.ProjectController;
import ru.koptev.taskmanager.controller.SystemController;
import ru.koptev.taskmanager.controller.TaskController;
import ru.koptev.taskmanager.controller.UserController;
import ru.koptev.taskmanager.enumerated.Role;
import ru.koptev.taskmanager.repository.ProjectRepository;
import ru.koptev.taskmanager.repository.TaskRepository;
import ru.koptev.taskmanager.repository.UserRepository;
import ru.koptev.taskmanager.service.ProjectService;
import ru.koptev.taskmanager.service.ProjectTaskService;
import ru.koptev.taskmanager.service.TaskService;
import ru.koptev.taskmanager.service.UserService;
import ru.koptev.taskmanager.util.HashUtil;

import static ru.koptev.taskmanager.constant.TerminalConstant.*;

import java.util.Scanner;


public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final SystemController systemController = new SystemController();

    private final ProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final UserController userController = new UserController(userService);

    private Long session;

    {
        projectRepository.create("XProject", "descriptionProject3");
        projectRepository.create("AbstractProject1", "descriptionProject1");
        projectRepository.create("TestProject2", "descriptionProject2");
        taskRepository.create("task1", "descriptionTask1");
        taskRepository.create("task2", "descriptionTask2");
        taskRepository.create("task3", "descriptionTask3");
        taskRepository.create("task4", "descriptionTask5");
        userRepository.create("ADMIN", HashUtil.saltHashMD5("qwerty123"), Role.ADMIN);
        userRepository.create("TEST", HashUtil.saltHashMD5("345qwerty123"), Role.USER);
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        Application application = new Application();
        String inputString = "";
        application.systemController.displayWelcome();
        while (!EXIT.equals(inputString)) {
            inputString = scanner.nextLine();
            application.systemController.logCommand(inputString);
            application.run(inputString);
        }
    }

    public void run(String param) {
        if (param == null || param.isEmpty()) return;
        switch (param) {
            case HELP:
                systemController.displayHelp();
                return;
            case ABOUT:
                systemController.displayAbout();
                return;
            case VERSION:
                systemController.displayVersion();
                return;
            case EXIT:
                systemController.displayExit();
                return;
            case COMMAND_HISTORY:
                systemController.commandHistory();
                return;

            case PROJECT_CREATE:
                projectController.create(session);
                return;
            case PROJECT_CLEAR:
                projectController.clear(session);
                return;
            case PROJECT_LIST:
                projectController.findAll(session);
                return;
            case PROJECT_VIEW_BY_ID:
                projectController.viewById(session);
                return;
            case PROJECT_VIEW_BY_NAME:
                projectController.viewByName(session);
                return;
            case PROJECT_REMOVE_BY_ID:
                projectController.removeById(session);
                return;
            case PROJECT_REMOVE_BY_NAME:
                projectController.removeByName(session);
                return;
            case PROJECT_UPDATE_BY_ID:
                projectController.updateById(session);
                return;
            case PROJECT_REMOVE_WITH_TASKS:
                projectController.removeProjectWithTasks(session);
                return;

            case TASK_CREATE:
                taskController.create(session);
                return;
            case TASK_CLEAR:
                taskController.clear(session);
                return;
            case TASK_LIST:
                taskController.findAll(session);
                return;
            case TASK_VIEW_BY_ID:
                taskController.viewById(session);
                return;
            case TASK_VIEW_BY_NAME:
                taskController.viewByName(session);
                return;
            case TASK_REMOVE_BY_ID:
                taskController.removeById(session);
                return;
            case TASK_REMOVE_BY_NAME:
                taskController.removeByName(session);
                return;
            case TASK_UPDATE_BY_ID:
                taskController.updateById(session);
                return;
            case TASK_LIST_BY_PROJECT_ID:
                taskController.listTaskByProjectIds(session);
                return;
            case TASK_ADD_TO_PROJECT_BY_IDS:
                taskController.addTaskToProjectByIds(session);
                return;
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                taskController.removeTaskFromProjectByIds(session);
                return;

            case USER_AUTHENTICATION:
                session = userController.authentication();
                return;
            case USER_LOGOFF:
                session = userController.logOff(session);
                return;
            case USER_CREATE:
                userController.create();
                return;
            case USER_CLEAR:
                userController.clear();
                return;
            case USER_LIST:
                userController.list();
                return;
            case USER_VIEW_CURRENT_PROFILE:
                userController.viewProfile(session);
                return;
            case USER_VIEW_BY_INDEX:
                userController.viewByIndex();
                return;
            case USER_VIEW_BY_ID:
                userController.viewById();
                return;
            case USER_REMOVE_BY_INDEX:
                userController.removeByIndex();
                return;
            case USER_REMOVE_BY_LOGIN:
                userController.removeByLogin();
                return;
            case USER_UPDATE_CURRENT_PROFILE:
                userController.updateProfile(session);
                return;
            case USER_UPDATE_BY_ID:
                userController.updateById();
                return;
            case USER_UPDATE_BY_LOGIN:
                userController.updateByLogin();
                return;

            default:
                systemController.displayError();
                return;
        }
    }

}
